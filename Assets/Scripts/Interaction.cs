﻿using System.Collections;
using System.Collections.Generic;
using Assets.ExampleAssets.BleibDaheim;
using UnityEngine;

public class Interaction : MonoBehaviour
{

    enum SelectedInteraction
    {
        GrabNDrop,
        Supplies,
        DenyAction
    }
    [SerializeField]
    private int T_Paper = 5;
    [SerializeField]
    private int FoodAmount =5;

    public GameObject actualTarget { private set; get; }
    private Karma karmaScript;
    private bool btnDown = false;

    private SelectedInteraction interaction = SelectedInteraction.GrabNDrop;
    private bool _isDragging;
    public bool isDragging
    {
        get => _isDragging;
        set => _isDragging = interaction == SelectedInteraction.GrabNDrop && value;
    }

    // Start is called before the first frame update
    void Start()
    {
        karmaScript = gameObject.GetComponent<Karma>();
    }

    void Restock()
    {
        T_Paper = 5;
        FoodAmount = 5;
    }

    public bool IsBeingDragged(GameObject go)
    {
        return isDragging && go.Equals(actualTarget);
    }
    public void StartInteract(GameObject interactObj, bool rightclick = false)
    {
        if (btnDown)
        {
            return;
        }

        var store = interactObj.GetComponent<Store>();
        if (store != null)
        {
            Restock();
            return;
        }
        btnDown = true;
        if (actualTarget != null) actualTarget.GetComponent<Resetable>().ResetInteraction();
        actualTarget = interactObj;
        var person = interactObj.GetComponent<Person>();
        if (person != null && !person.isAtHome)
        {
            isDragging = true;
            return;
        }
        Supplie(rightclick);
        isDragging = false;
    }

    void Supplie(bool rightclick)
    {
        var person =actualTarget.GetComponent<Person>();
        if (person == null) return;
        
        if (rightclick)
        {
            if (T_Paper >0)
            {
                T_Paper -= 1;
                
            }else
                return; //TODO: cannot supplie
        }
        else
        {
            if (FoodAmount > 0)
            {
                FoodAmount -= 1;
                
            }else
                return; //TODO: cannot supplie
        }
        person.GetSupplies(rightclick);
    }

    public void StopInteract()
    {
        if (!btnDown)
        {
            return;
        }

        btnDown = false;
        if (isDragging && actualTarget.GetComponent<Draggable>().isOverHome)
        {
            GameManager.instance.IncreaseTime(2.0f);
            actualTarget.GetComponent<Person>().MoveInHouse();
        }else if (isDragging)
        {
            var newpos = actualTarget.transform.position;
            newpos.y = actualTarget.GetComponent<Person>().yValue;
            actualTarget.transform.position = newpos;
        }
        isDragging = false;
        actualTarget = null;
    }

    // Update is called once per frame
    void Update()
    {
    }
}

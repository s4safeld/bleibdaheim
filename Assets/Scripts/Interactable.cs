﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using Color = UnityEngine.Color;

public class Interactable : MonoBehaviour
{
    // Start is called before the first frame update

    private bool needSupplies = false;
    private float mood = 10;
    private bool highlited = false;
    private SpriteRenderer spriteRenderer;
    void Start()
    {
        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
    }


    public void Highlight(Color? highlightColor = null) // TODO: anpassen
    {
        if (highlightColor != null)
        {
            spriteRenderer.color = highlightColor.Value;
            highlited = true;
        }
        else
        {
            spriteRenderer.color = Color.white;
            highlited = false;
        }
    }

    public void SoftHighlight(Color? color = null)
    {
        if (!highlited)
        {
            spriteRenderer.color = color ?? Color.white;
        }
    }


}

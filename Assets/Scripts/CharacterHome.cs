﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.ExampleAssets.BleibDaheim;
using UnityEngine;

public class CharacterHome : MonoBehaviour, DenyableAction
{
    // Start is called before the first frame update
    public GameObject[] inhabitants;
    private IList<GameObject> Allinhabitants;
    public IList<GameObject> inhabitantsAtHome;
    private Interaction interactionScript;
    private SpriteRenderer spriteRenderer;
    private Vector3 spanwpoint;
    public Interactable _interactable { private set; get; }
    private Bubble bubbles;
    private IList<Window> windows;

    public bool makeParty = false;
    public bool needFood = false;

    void Start()
    {
        bubbles = gameObject.GetComponentInChildren<Bubble>();
        spanwpoint = new Vector3(transform.position.x,transform.position.y);
        _interactable = GetComponent<Interactable>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        inhabitantsAtHome = new List<GameObject>();
        Allinhabitants = new List<GameObject>();
        windows = gameObject.GetComponentsInChildren<Window>();
        foreach (var person in inhabitants)
        {
            var newperson = Instantiate(person);
            newperson.GetComponent<Draggable>().SetHome(this);
            interactionScript = FindObjectOfType<Interaction>();
            spanwpoint.z = newperson.transform.position.z;
            newperson.transform.position = spanwpoint;
            Allinhabitants.Add(newperson);
            newperson.GetComponent<Person>().MoveInHouse();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!interactionScript.isDragging)
        {
            _interactable.Highlight();
        }
        CheckHouse();
    }

    public Window GetFreeWindow()
    {
        Window wnd = windows.FirstOrDefault(x => !x.actuallyUsed);
        return windows.Contains(wnd) ? wnd : null;
    }
    void CheckHouse()
    {
        if (inhabitantsAtHome.Count == 0)
        {
            bubbles.gameObject.GetComponent<SpriteRenderer>().sprite = null;
            needFood = false;
            return;
        }
        float partyValue = 0;
        foreach (var inhabitant in inhabitantsAtHome)
        {
            var person = inhabitant.GetComponent<Person>();
            partyValue += person.needForParty;
            needFood = person.isHungry ? person.isHungry : needFood;
        }
        partyValue /= Allinhabitants.Count;

        makeParty = partyValue >= 0.7f;
        if (!makeParty && needFood)
        {
            bubbles.gameObject.GetComponent<SpriteRenderer>().sprite = bubbles.hunger();
        }

        else if (makeParty)
        {
            bubbles.gameObject.GetComponent<SpriteRenderer>().sprite = bubbles.party();
        }
        else
        {
            bubbles.gameObject.GetComponent<SpriteRenderer>().sprite = null;
        }
    }

    public void AddInhabitant(GameObject inhabitant)
    {
        Allinhabitants.Add(inhabitant);
    }

    void OnMouseDown()
    {
        interactionScript.StartInteract(gameObject);
    }
    void OnTriggerEnter2D(Collider2D col)
    {
        var interactable = col.gameObject.GetComponent<Draggable>();
        if (Allinhabitants.Contains(col.gameObject) && interactable.mouseDown && interactionScript.isDragging)
        {
            interactable.isOverHome = true;
           _interactable.Highlight(Color.red);
        }
    }

    void OnTriggerExit2D(Collider2D col)
    {
        var interactable = col.gameObject.GetComponent<Draggable>();
        _interactable.Highlight();
        interactable.isOverHome = false;
    }

    void OnTriggerStay2D(Collider2D col)
    {
        var interactable = col.gameObject.GetComponent<Draggable>();
        if (Allinhabitants.Contains(col.gameObject) && interactable.mouseDown && interactionScript.isDragging)
        {

            interactable.isOverHome = true;
            _interactable.Highlight(Color.red);
        }
    }

    public void DenyAction()
    {
        makeParty = false;
    }

    public void BackAtHome(Person person)
    {
        if (Allinhabitants.Contains(person.gameObject))
        {
            inhabitantsAtHome.Add(person.gameObject);
        }
    }

    public void LeaveHome(Person person)
    {
        if (Allinhabitants.Contains(person.gameObject))
        {
            inhabitantsAtHome.Remove(person.gameObject);
        }
    }
}

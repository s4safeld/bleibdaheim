﻿using System.CodeDom;
using System.Collections;
using System.Collections.Generic;
using Assets.ExampleAssets.BleibDaheim;
using UnityEngine;

public class Ai_Movement : MonoBehaviour
{
    // Start is called before the first frame update
    public float speed;
    private Person person;
    private float targetX;
    void Start()
    {
        speed = 1;
        person = gameObject.GetComponent<Person>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!person.isAtHome)
        {
            if (person.currentAction != Enums.Actions.Food && person.currentAction != Enums.Actions.Toilettpaper)
            {
                if (IsAtTarget())
                {
                    moveDistance(Random.Range(50,600),true);
                }
            }
            move(speed);
        }
    }

    void move(float speed)
    {
        var deltaDist =speed * Time.deltaTime;
        if (!IsAtTarget())
        {
            if (transform.position.x < targetX)
            {
                transform.position += new Vector3(deltaDist, 0, 0);
            }
            else
            {
                transform.position -= new Vector3(deltaDist, 0, 0);
            }
        }
    }

    bool IsAtTarget()
    {
        if (Mathf.Abs(person.transform.position.x -targetX) <= 0.1)
        {
            return true;
        }
        return false;
    }

    public void moveDistance(float distance, bool wander)
    {
        targetX = transform.position.x + distance;
        if (wander)
        {
            targetX = transform.position.x + Random.value >= 0.55f ? distance : -distance;
        }
    }
    public void moveDistance(float targetX)
    {
        this.targetX = targetX;
    }
}

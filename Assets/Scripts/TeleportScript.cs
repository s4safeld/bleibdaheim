﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class TeleportScript : MonoBehaviour
{
    // Start is called before the first frame update
    private BackgroundLoop bl;
    private Camera _camera;
    private float halfWidth;
    private GameObject clone = null;
    void Start()
    {
        bl = FindObjectOfType<BackgroundLoop>();
        _camera = bl.gameObject.GetComponent<Camera>();
        halfWidth = GetComponent<SpriteRenderer>().bounds.extents.x;
        _camera.gameObject.GetComponent<BackgroundLoop>().TeleportDelegate += Teleport;
    }

    // Update is called once per frame
    void Update()
    {
        Teleport(bl.gameObject.transform.position);
    }

    void Teleport(Vector3 cameraPos)
    {
        float cameraDist = transform.position.x - cameraPos.x;
        var vec = new Vector3(transform.position.x - halfWidth, transform.position.y, transform.position.z);
        var vwPLeft = _camera.WorldToViewportPoint(vec);
        vec.x = transform.position.x + halfWidth;
        var vwPright = _camera.WorldToViewportPoint(vec);
        if (vwPright.x < 0 || vwPLeft.x > 1)
        {
            if (Mathf.Abs(cameraDist) > bl.backgroundWidth / 2)
            {
                vec.y = 0;
                vec.z = 0;
                if (transform.position.x > _camera.transform.position.x)
                {
                    vec.x = -bl.backgroundWidth;
                    
                    transform.position += vec;
                    return;
                }

                else if (transform.position.x < _camera.transform.position.x)
                {
                    vec.x = bl.backgroundWidth;
                    transform.position += vec;
                    return;
                }
            }
        }
    }
}


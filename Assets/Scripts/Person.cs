﻿
using System;
using System.Collections;
using System.Security.Cryptography;
using Assets.ExampleAssets.BleibDaheim;
using UnityEngine;
using UnityEngine.UIElements;
using Random = UnityEngine.Random;

public class Person : MonoBehaviour, DenyableAction, Resetable
{
    public CharacterHome home;
    private Window currentWindow = null;
    public float mood;
    public UnityEngine.UI.Slider slider;
    private Enums.Actions _currentAction;
    public Enums.Actions currentAction
    {
        get => _currentAction;
        private set
        {
            slider.gameObject.SetActive(false);
            stateTime = _currentAction == value ? stateTime : 0.0f;
            if (value == Enums.Actions.Nothing)
            {
                bubble.GetComponent<SpriteRenderer>().sprite = null;
                
            }
            _currentAction = value;
        }
    }
    public bool isAtHome = false;
    public float needForParty { get; set; }
    private float stepTime = 0.5f;
    private float currentTime = 0.0f;
    private Bubble bubble;
    private Ai_Movement movement;
    private float personalStateWaitingTime = 5.0f;
    private float waitforActionTime = 50.0f;

    public float yValue;
    public bool isBeingDragged = false;
    public bool isHungry
    {
        get { return currentAction == Enums.Actions.Food; }
    }

    private float stateTime = 0.0f;

    // Start is called before the first frame update
    void Start()
    {
        movement = GetComponent<Ai_Movement>();
        bubble = GetComponentInChildren<Bubble>();
        currentAction = Enums.Actions.Food;
    }

    // Update is called once per frame
    void Update()
    {
        currentTime += Time.deltaTime;
        stateTime += Time.deltaTime;
        if (currentTime >= stepTime)
        {
            switch (currentAction)
            {
                case Enums.Actions.Nothing:
                    StateNothing();
                    break;
                case Enums.Actions.Food:
                case Enums.Actions.Toilettpaper:
                    NeedSupplies();
                    break;
                case Enums.Actions.Shopping:
                    StateShopping();
                    break;
                case Enums.Actions.Party:
                    break;
            }
            currentTime = 0.0f;
        }
    }

    public void GetSupplies(bool TP)
    {
        if (TP && currentAction == Enums.Actions.Toilettpaper )
        {
            GotSupplied();
            GameManager.instance.IncreaseTime(4);
        }else if (!TP && currentAction == Enums.Actions.Food)
        {
            GotSupplied();
            GameManager.instance.IncreaseTime(4);
        }
        else
        {
            
            //Todo: punishment?
        }
    }


    public void LeaveHouse()
    {
        home.LeaveHome(this);
        isAtHome = false;
        Show(true);
        yValue = -Random.Range(7.0f, 7.3f);
        transform.position = new Vector3(transform.position.x,yValue,transform.position.z);
        gameObject.GetComponent<SpriteRenderer>().sortingLayerName = "Foreground";
    }

    void Show(bool show)
    {
        gameObject.GetComponent<SpriteRenderer>().enabled = show;
        if (!bubble)
        {
            bubble = GetComponentInChildren<Bubble>();
        }
        bubble.gameObject.GetComponent<SpriteRenderer>().enabled = show;
        gameObject.GetComponent<BoxCollider2D>().enabled = show;
    }


    public void MoveInHouse()
    {
        home.BackAtHome(this);
        isAtHome = true;
        Show(false);

        gameObject.GetComponent<SpriteRenderer>().sortingLayerName = "Houses";

    }

    public void DenyAction()
    {
        currentAction = Enums.Actions.Nothing;
    }

    void NeedSupplies()
    {
        
        if (!isAtHome)//go shopping
        {
            StopAllCoroutines();
            bubble.GetComponent<SpriteRenderer>().sprite = bubble.party();
            GetSupermarket();
        }
        else
        {
            StartCoroutine(Food());
        }

    }

    void GotSupplied()
    {

        Show(false);
        currentWindow.Close();
        coRtFoodStarted = false;
        slider.gameObject.SetActive(false);
        currentAction = Enums.Actions.Nothing;
        StopAllCoroutines();

    }

    private bool coRtFoodStarted = false;
    private IEnumerator Food()
    {
        if (!coRtFoodStarted)
        {
            currentWindow = home.GetFreeWindow();
            while (currentWindow == null )
            {
                currentWindow = home.GetFreeWindow();
                yield break;
            }
            Show(true);
            if (currentWindow == null)
            {
                Debug.Log("NULL");
            }
            gameObject.transform.position = currentWindow.GetLocation();
            currentWindow.Open();

            if (currentAction == Enums.Actions.Toilettpaper)
            {
                bubble.GetComponent<SpriteRenderer>().sprite = bubble.paper();
            }else
                bubble.GetComponent<SpriteRenderer>().sprite = bubble.hunger();
            coRtFoodStarted = true;
            slider.gameObject.SetActive(true);
            float time = 0.0f;
            var x = slider;
            x.value = 1;
            while (time < waitforActionTime)
            {
                x.value = 1 - time / waitforActionTime;
                time += Time.deltaTime;
                yield return null;
            }
            GameManager.instance.DecreaseTime(2);
            GotSupplied();
        }
    }

    private void Party()
    {

    }

    void GetSupermarket()
    {
        var stores = FindObjectsOfType<Store>();
        float distance = Single.NaN;
        float ClosestStore = transform.position.x;
        foreach (var store in stores)
        {
            if (float.IsNaN(distance))
            {
                distance = store.GetEntrance().x - gameObject.transform.position.x;
                ClosestStore = store.GetEntrance().x;
            }
            else
            {
                var dist = store.GetEntrance().x - gameObject.transform.position.x;
                if (Mathf.Abs(distance) > Mathf.Abs(dist) )
                {
                    distance = dist;
                    ClosestStore = store.GetEntrance().x;
                }
            }
        } movement.moveDistance(ClosestStore);
    }

    void StateNothing()
    {

        if (stateTime >= personalStateWaitingTime)
        {

            stateTime = 0.0f;
            personalStateWaitingTime = Random.Range(4.0f, 7.0f);
            if (Random.value > 0.4f)
                if (Random.value > 0.4f)
                {
                    currentAction = Random.value > 0.5f ? Enums.Actions.Food : Enums.Actions.Toilettpaper;
                }
                else
                    LeaveHouse();

            return;
        }
    }
    void OnMouseOver()
    {
        Debug.Log("Asdasdasd");
    }

    private void StateShopping()
    {
        LeaveHouse();
    }

    private void StateParty()
    {

    }

    public void ResetInteraction()
    {
        isBeingDragged = false;
        var pos = transform.position;
        pos.y = yValue;
        transform.position = pos;
    }
}

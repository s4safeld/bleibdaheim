﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Store : MonoBehaviour 
{
    [SerializeField]
    private Vector3 entrance;

    private Interaction interactionScript;

    // Start is called before the first frame update
    void Start()
    {
        interactionScript = FindObjectOfType<Interaction>();

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public Vector3 GetEntrance()
    {
        return gameObject.transform.position + entrance;
    }

    void OnMouseDown()
    {
        interactionScript.StartInteract(gameObject);
    }

    void OnMouseUp()
    {
        interactionScript.StopInteract();
    }
}
